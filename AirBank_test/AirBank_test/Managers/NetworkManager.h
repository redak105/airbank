//
//  NetworkManager.h
//  Example_project
//
//  Created by Radek Zmeskal on 23/09/15.
//  Copyright (c) 2015 Radek Zmeskal. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Network manager to care all request
 */
@interface NetworkManager : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (id)sharedManager;

/**
 *  get list of transaction
 *
 *  @param success      success callback
 *  @param errorHandler error callback handler
 */
-(void)getListWithSuccess:(void (^)(NSArray* response)) success failure:(void (^)(NSError* error)) errorHandler;

/**
 *  get detail of transaction
 *
 *  @param identifier   id of trnsactions
 *  @param success      success callback
 *  @param errorHandler error callback handler
 */
-(void)getDetailWithID:(NSNumber*) identifier success:(void (^)(NSDictionary* response)) success failure:(void (^)(NSError* error)) errorHandler;

@end
