//
//  DataManager.m
//  Example_project
//
//  Created by Radek Zmeskal on 23/09/15.
//  Copyright (c) 2015 Radek Zmeskal. All rights reserved.
//


#import "DataManager.h"
#import <MBProgressHUD.h>
#import "NetworkManager.h"
#import "Transaction.h"
#import "TransactionDetail.h"
#import "AppDelegate.h"
#import <UIKit/UIKit.h>

@implementation DataManager

+ (id)sharedManager
{
    static DataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void)loadTransactionsWithViewControler:(UIViewController*) viewController success:(void (^)(NSArray* payments)) success
{
    // show progress
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:[viewController view]];
    [[viewController view] addSubview:hud];
    [hud show:YES];
    
    // request
    [[NetworkManager sharedManager] getListWithSuccess:^(NSArray *response) {
        
        NSMutableArray *payments = [[NSMutableArray alloc] initWithCapacity:[response count]];
        
        // serialize objects
        for (NSDictionary *payment in response)
        {
            Transaction *paymentNew = [[Transaction alloc] initWithDictionary:payment];
            
            [payments addObject:paymentNew];
        }
        
        [hud hide:YES];
        
        success(payments);
        
    } failure:^(NSError *error) {
        [hud hide:YES];
        
        // show error message
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[error localizedFailureReason] message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [[viewController navigationController] popViewControllerAnimated:YES];
        }];
        
        [alert addAction:action];
        
        [viewController presentViewController:alert animated:YES completion:nil];
    }];
}

-(void)loadTransactionDetailWithViewControler:(UIViewController*) viewController transaction:(Transaction*) transaction success:(void (^)()) success
{
    // show progress
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:[viewController view]];
    [[viewController view] addSubview:hud];
    [hud show:YES];
    
    // request
    [[NetworkManager sharedManager] getDetailWithID:[transaction identificator] success:^(NSDictionary *response)
    {
        TransactionDetail *detail = [[TransactionDetail alloc] initWithDictionary:response];

        [transaction setDetail:detail];
                
        [hud hide:YES];
        
        success();
        
    } failure:^(NSError *error) {
        [hud hide:YES];
        
        // show error message
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[error localizedFailureReason] message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [[viewController navigationController] popViewControllerAnimated:YES];
        }];
        
        [alert addAction:action];
        
        [viewController presentViewController:alert animated:YES completion:nil];
    }];
    
}


@end
