//
//  DataManager.h
//  Example_project
//
//  Created by Radek Zmeskal on 23/09/15.
//  Copyright (c) 2015 Radek Zmeskal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Transaction.h"

/**
 *  Manager for data objects
 */
@interface DataManager : NSObject

/**
 *  shared instance
 *
 *  @return class
 */
+ (id)sharedManager;

/**
 *  load transactions from server
 *
 *  @param viewController actual view controller
 *  @param success        success callback
 */
-(void)loadTransactionsWithViewControler:(UIViewController*) viewController success:(void (^)(NSArray* payments)) success;

/**
 *  load transaction detail
 *
 *  @param viewController actual view controller
 *  @param transaction    transaction to load detail
 *  @param success        success callback
 */
-(void)loadTransactionDetailWithViewControler:(UIViewController*) viewController transaction:(Transaction*) transaction success:(void (^)()) success;

@end
