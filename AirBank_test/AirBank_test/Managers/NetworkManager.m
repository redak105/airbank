//
//  NetworkManager.m
//  Example_project
//
//  Created by Radek Zmeskal on 23/09/15.
//  Copyright (c) 2015 Radek Zmeskal. All rights reserved.
//

#import "NetworkManager.h"
#import <AFNetworking.h>


#define URL_LIST @"https://demo9815075.mockable.io/transactions"
#define URL_DETAIL @"https://demo9815075.mockable.io/transactions"

@implementation NetworkManager

+ (id)sharedManager
{
    static NetworkManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void)getListWithSuccess:(void (^)(NSArray* response)) success failure:(void (^)(NSError* error)) errorHandler;
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    NSDictionary *parameters = nil;
    
    [manager GET:URL_LIST parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
     {
         NSDictionary *json = (NSDictionary*)responseObject;
         
         // test requeest status
         NSNumber *status = [json objectForKey:@"status"];
         if (![status boolValue])
         {
             // show error
             NSError *error = [NSError errorWithDomain:[json objectForKey:@"error"] code:0 userInfo:nil];
             errorHandler(error);
             
             return;
         }
         
         NSArray *items = [json objectForKey:@"items"];
        
         success(items);
         
     } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
         errorHandler(error);
     }];

}

-(void)getDetailWithID:(NSNumber*) identifier success:(void (^)(NSDictionary* response)) success failure:(void (^)(NSError* error)) errorHandler
{
    NSString *url = [NSString stringWithFormat:@"%@/%d", URL_DETAIL, [identifier integerValue]];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    NSDictionary *parameters = nil;
    
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
     {
         NSDictionary *json = (NSDictionary*)responseObject;
         
         // test requeest status
         NSNumber *status = [json objectForKey:@"status"];
         if (![status boolValue])
         {
             // show error
             NSError *error = [NSError errorWithDomain:[json objectForKey:@"error"] code:0 userInfo:nil];
             errorHandler(error);
             
             return;
         }
         
         NSDictionary *contraAccount = [json objectForKey:@"contraAccount"];
         
         success(contraAccount);
         
     } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
         errorHandler(error);
     }];
}

@end
