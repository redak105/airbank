//
//  colors.h
//  AirBank_test
//
//  Created by Radek Zmeskal on 30/09/15.
//  Copyright © 2015 Radek Zmeskal. All rights reserved.
//

#ifndef colors_h
#define colors_h

#define COLOR_FILTER [UIColor colorWithRed:145.0/255.0 green:145.0/255.0 blue:155.0/255.0 alpha:1.0]
#define COLOR_FILTER_SELECT [UIColor colorWithRed:185.0/255.0 green:185.0/255.0 blue:194.0/255.0 alpha:1.0]
#define COLOR_BACKGROUND [UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:130.0/255.0 alpha:1.0]

#endif /* colors_h */
