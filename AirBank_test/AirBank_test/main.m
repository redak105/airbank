//
//  main.m
//  AirBank_test
//
//  Created by Radek Zmeskal on 28/09/15.
//  Copyright © 2015 Radek Zmeskal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
