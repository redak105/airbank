//
//  Payment.h
//  AirBank_test
//
//  Created by Radek Zmeskal on 28/09/15.
//  Copyright © 2015 Radek Zmeskal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionDetail.h"

/**
 *  Transaction class
 */
@interface Transaction : NSObject

/**
 *  Transaction identificator(ID)
 */
@property NSNumber *identificator;
/**
 *  Transaction direction Incoming/Outgoing
 */
@property NSString *direction;
/**
 *  Transaction amount
 */
@property NSNumber *amountInAccountCurrency;

/**
 *  Transaction detail
 */
@property TransactionDetail *detail;


/**
 *  Inicializer with dictionary
 *
 *  @param dictionary source dictionary
 *
 *  @return instance
 */
-(instancetype)initWithDictionary:(NSDictionary*) dictionary;

@end
