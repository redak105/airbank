//
//  TransactionDetail.h
//  AirBank_test
//
//  Created by Radek Zmeskal on 01/10/15.
//  Copyright © 2015 Radek Zmeskal. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Transaction detail
 */
@interface TransactionDetail : NSObject

/**
 *  Transaction account number
 */
@property NSString *accountNumber;
/**
 *  Transaction account name
 */
@property NSString *accountName;
/**
 *  transaction bank code
 */
@property NSString *bankCode;

/**
 *  Inicializer with dictionary
 *
 *  @param dictionary source dictionary
 *
 *  @return instance
 */
-(instancetype)initWithDictionary:(NSDictionary*) dictionary;

@end
