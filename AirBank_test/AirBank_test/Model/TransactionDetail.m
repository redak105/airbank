//
//  TransactionDetail.m
//  AirBank_test
//
//  Created by Radek Zmeskal on 01/10/15.
//  Copyright © 2015 Radek Zmeskal. All rights reserved.
//

#import "TransactionDetail.h"

@implementation TransactionDetail

-(instancetype)initWithDictionary:(NSDictionary*) dictionary
{
    self = [super init];
    if (self)
    {
        [self setAccountNumber:[dictionary objectForKey:@"accountNumber"]];
        [self setAccountName:[dictionary objectForKey:@"accountName"]];
        [self setBankCode:[dictionary objectForKey:@"bankCode"]];
    }
    return self;
}


@end
