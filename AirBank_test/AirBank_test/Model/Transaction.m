//
//  Payment.m
//  AirBank_test
//
//  Created by Radek Zmeskal on 28/09/15.
//  Copyright © 2015 Radek Zmeskal. All rights reserved.
//

#import "Transaction.h"

@implementation Transaction

-(instancetype)initWithDictionary:(NSDictionary*) dictionary
{
    self = [super init];
    if (self)
    {
        [self setIdentificator:[dictionary objectForKey:@"id"]];
        [self setDirection:[dictionary objectForKey:@"direction"]];
        [self setAmountInAccountCurrency:[dictionary objectForKey:@"amountInAccountCurrency"]];
    }
    return self;
}

@end
