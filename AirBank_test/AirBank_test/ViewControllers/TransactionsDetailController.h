//
//  TransactionsDetailController.h
//  AirBank_test
//
//  Created by Radek Zmeskal on 30/09/15.
//  Copyright © 2015 Radek Zmeskal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"

/**
 *  Transaction detail controller
 */
@interface TransactionsDetailController : UIViewController

/**
 *  view of transaction detail
 */
@property (weak, nonatomic) IBOutlet UIView *viewDetail;

/**
 *  image to show type of transaction
 */
@property (weak, nonatomic) IBOutlet UIImageView *imageType;

/**
 *  label to show amount of transaction
 */
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;
/**
 *  label to show type of transaction
 */
@property (weak, nonatomic) IBOutlet UILabel *labelType;
/**
 *  label to show account type of transaction
 */
@property (weak, nonatomic) IBOutlet UILabel *labelAccountType;
/**
 *  label to show account name of transaction
 */
@property (weak, nonatomic) IBOutlet UILabel *labelAccountName;
/**
 *  label to show bank code of transaction
 */
@property (weak, nonatomic) IBOutlet UILabel *labelBankCode;


/**
 *  transaction to show detail
 */
@property Transaction *transaction;

@end
