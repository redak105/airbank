//
//  TransactionsDetailController.m
//  AirBank_test
//
//  Created by Radek Zmeskal on 30/09/15.
//  Copyright © 2015 Radek Zmeskal. All rights reserved.
//

#import "TransactionsDetailController.h"
#import "DataManager.h"
#import "colors.h"

@interface TransactionsDetailController ()

@end

@implementation TransactionsDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:@"Detail"];
    
    [[self view] setBackgroundColor:COLOR_BACKGROUND];
    
    // test if detail is loaded
    if (([[self transaction] detail] != nil) && ![[[self transaction] detail] isKindOfClass:[NSNull class]])
    {
        [self initView];
        return;
    }
    
    // load detail of transaction
    [[DataManager sharedManager] loadTransactionDetailWithViewControler:self transaction:[self transaction] success:^{
        [self initView];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Functions

/**
 *  setup view base on transaction
 */
-(void)initView
{
    [[self labelAmount] setText:[NSString stringWithFormat:@"%.2f Kc", [[[self transaction] amountInAccountCurrency] floatValue]]];
    
    if ([[[self transaction] direction] isEqualToString:@"INCOMING"])
    {
        [[self labelType] setText:@"Incoming"];
        [[self imageType] setImage:[UIImage imageNamed:@"incoming"]];
        
    }
    if ([[[self transaction] direction] isEqualToString:@"OUTGOING"])
    {
        [[self labelType] setText:@"Outgoing"];
        [[self imageType] setImage:[UIImage imageNamed:@"outgoing"]];
    }
    
    [[self labelAccountName] setText:[[[self transaction] detail] accountName]];
    [[self labelAccountType] setText:[[[self transaction] detail] accountNumber]];
    [[self labelBankCode] setText:[[[self transaction] detail] bankCode]];
    
    [[self viewDetail] setHidden:NO];
}


@end
