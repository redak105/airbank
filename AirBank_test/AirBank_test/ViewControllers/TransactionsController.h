//
//  PaymentsController.h
//  AirBank_test
//
//  Created by Radek Zmeskal on 28/09/15.
//  Copyright © 2015 Radek Zmeskal. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Transaction controller
 */
@interface TransactionsController : UITableViewController
{
    /**
     *  filter ON/OFF
     */
    BOOL filter;
    /**
     *  filter word to filtre array
     */
    NSString *filterWord;
    
    /**
     *  source array of all payments
     */
    NSArray *arrayTransactions;
    
    /**
     *  filtered array
     */
    NSArray *arrayFiltered;
}

@end
