//
//  PaymentsController.m
//  AirBank_test
//
//  Created by Radek Zmeskal on 28/09/15.
//  Copyright © 2015 Radek Zmeskal. All rights reserved.
//

#import "TransactionsController.h"
#import "DataManager.h"
#import "Transaction.h"
#import "colors.h"
#import "TransactionsDetailController.h"

@interface TransactionsController ()

@end

@implementation TransactionsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // setup apperance
    [[[self navigationController] navigationBar] setBarTintColor:COLOR_FILTER_SELECT];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [[self tableView] setBackgroundColor:COLOR_BACKGROUND];
    
    [[self tableView] setTableFooterView:[[UIView alloc] init]];
    
    // load data
    [[DataManager sharedManager] loadTransactionsWithViewControler:self success:^(NSArray *payments) {
        arrayTransactions = payments;
        arrayFiltered = arrayTransactions;
        
        [[self tableView] reloadData];
    }];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self setTitle:@"Back"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setTitle:@"List"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            if (filter)
            {
                return 3;
            }
            else {
                return 1;
            }
            break;
        case 1:
            return [arrayFiltered count];
            
        default:
            break;
    };
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if ([indexPath section] == 0)
    {
        // filter cell
        if ([indexPath row] == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"cellFilters" forIndexPath:indexPath];
            
            [cell setBackgroundColor:COLOR_FILTER];
            
            UILabel *label = (UILabel*)[cell viewWithTag:-1];
            UIImageView *image = (UIImageView*)[cell viewWithTag:-2];
            
            [label setTextColor:[UIColor whiteColor]];
            
            if (filter)
            {
                [image setImage:[UIImage imageNamed:@"filterOFF"]];
            }
            else {
                [image setImage:[UIImage imageNamed:@"filterON"]];
            }
            
            return cell;
        }
        
        // filter item cells
        cell = [tableView dequeueReusableCellWithIdentifier:@"cellFilter" forIndexPath:indexPath];
        UILabel *label = (UILabel*)[cell viewWithTag:-1];
        UIImageView *image = (UIImageView*)[cell viewWithTag:-2];
        
        [cell setBackgroundColor:COLOR_FILTER_SELECT];
        
        // test type of filter
        if ([indexPath row] == 1)
        {
            [label setText:@"Incoming"];
            
            if ([filterWord isEqualToString:@"INCOMING"])
            {
                [image setImage:[UIImage imageNamed:@"filterOK"]];
            }
            else {
                [image setImage:nil];
            }
        }
        
        if ([indexPath row] == 2)
        {
            [label setText:@"Outgoing"];
            
            if ([filterWord isEqualToString:@"OUTGOING"])
            {
                [image setImage:[UIImage imageNamed:@"filterOK"]];
            }
            else {
                [image setImage:nil];
            }
        }
                
    }
    
    // transactions cells
    if ([indexPath section] == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cellTransaction" forIndexPath:indexPath];
        
        [cell setTag:[indexPath row]];
            
        UIImageView *image = (UIImageView*)[cell viewWithTag:-1];
        UILabel *labelAmount = (UILabel*)[cell viewWithTag:-2];
        UILabel *labelType = (UILabel*)[cell viewWithTag:-3];
        
        Transaction *payment = [arrayFiltered objectAtIndex:[indexPath row]];
        
        [labelAmount setText:[NSString stringWithFormat:@"%.2f Kc", [[payment amountInAccountCurrency] floatValue]]];
        if ([[payment direction] isEqualToString:@"INCOMING"])
        {
            [labelType setText:@"Incoming"];
            [image setImage:[UIImage imageNamed:@"incoming"]];
           
        }
        if ([[payment direction] isEqualToString:@"OUTGOING"])
        {
            [labelType setText:@"Outgoing"];
            [image setImage:[UIImage imageNamed:@"outgoing"]];
        }
        
        return cell;
    }

    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section] == 0)
    {
        return 41;
    }
    if ([indexPath section] == 1)
    {
        return 61;
    }
    
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // filters
    if ([indexPath section] == 0)
    {
        // test click filters
        if ([indexPath row] == 0)
        {
            filter = !filter;
            
            NSArray *rows = @[ [NSIndexPath indexPathForItem:1 inSection:0], [NSIndexPath indexPathForItem:2 inSection:0]];
            
            if (!filter)
            {
                filterWord = nil;
                [[self tableView] deleteRowsAtIndexPaths:rows withRowAnimation:UITableViewRowAnimationTop];
            }
            else {
                [[self tableView] insertRowsAtIndexPaths:rows withRowAnimation:UITableViewRowAnimationTop];
            }
        }
        
        // filter array
        if ([indexPath row] == 1)
        {
            if ([filterWord isEqualToString:@"INCOMING"])
            {
                filterWord = nil;
            }
            else {
                filterWord = @"INCOMING";
            }
        }
        
        if ([indexPath row] == 2)
        {
            if ([filterWord isEqualToString:@"OUTGOING"])
            {
                filterWord = nil;
            }
            else {
                filterWord = @"OUTGOING";
            }
        }
        
        [self filterArray];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueDetail"])
    {
        TransactionsDetailController *controller = (TransactionsDetailController*)[segue destinationViewController];
        
        Transaction *transaction = [arrayFiltered objectAtIndex:[sender tag]];
        [controller setTransaction:transaction];
    }
}

#pragma mark - functions

/**
 *  filter array
 */
-(void)filterArray
{
    // empty filter
    if (filterWord == nil)
    {
        arrayFiltered = arrayTransactions;
    }
    
    // incoming
    if ([filterWord isEqualToString:@"INCOMING"])
    {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for (Transaction *transaction in arrayTransactions)
        {
            if ([[transaction direction] isEqualToString:@"INCOMING"])
            {
                [array addObject:transaction];
            }
        }
        
        arrayFiltered = array;
    }
    
    // outgoing
    if ([filterWord isEqualToString:@"OUTGOING"])
    {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for (Transaction *transaction in arrayTransactions)
        {
            if ([[transaction direction] isEqualToString:@"OUTGOING"])
            {
                [array addObject:transaction];
            }
        }
        
        arrayFiltered = array;
    }
    
    [[self tableView] reloadData];
}



@end
